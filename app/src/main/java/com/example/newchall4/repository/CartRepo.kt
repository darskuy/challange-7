package com.example.newchall4.repository

import androidx.lifecycle.LiveData
import com.example.newchall4.database.CartItemDao
import com.example.newchall4.item.CartItem

class CartRepository(private val cartDao: CartItemDao) {

    val allCartItems: LiveData<List<CartItem>> = cartDao.getAllCartItems()

    fun insertCartItem(cartItem: CartItem) {
        cartDao.insertCartItem(cartItem)
    }

    fun deleteCartItem(cartItem: CartItem) {
        cartDao.deleteCartItem(cartItem)
    }

    fun updateCartItem(cartItem: CartItem) {
        cartDao.updateCartItem(cartItem)
    }

    fun deleteAllCartItems() {
        cartDao.deleteAllCartItems()
    }

    fun getCartByFoodName(foodName:String):CartItem?{
        return cartDao.getCartItemByFoodName(foodName)
    }
}
