package com.example.newchall4.Adapter

import com.example.newchall4.databinding.ItemFoodBinding
import com.example.newchall4.menu.DataCategory
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

class CategoryAdapter(
    private val onItemClick: (DataCategory) -> Unit
) : RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    private val categoryItems: ArrayList<DataCategory> = arrayListOf()

    // Mengatur daftar kategori
    fun setCategoryItems(menuItems: List<DataCategory>) {
        categoryItems.clear()
        categoryItems.addAll(menuItems)
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemFoodBinding) :
        RecyclerView.ViewHolder(binding.root) {

        // Fungsi untuk binding data ke tampilan
        fun bind(categoryItem: DataCategory) {
            // Menggunakan Glide untuk memuat gambar dari URL ke ImageView
            Glide.with(itemView.context)
                .load(categoryItem.imageUrl)
                .into(binding.ivCategoryIcon)

            // Menetapkan kategori ke TextView
            binding.tvCategoryName.text = categoryItem.nama

            itemView.setOnClickListener {
                onItemClick(categoryItem)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            ItemFoodBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(categoryItems[position])
    }

    override fun getItemCount(): Int {
        return categoryItems.size
    }
}