package com.example.newchall4.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.example.newchall4.ActivityLoginAccount
import com.example.newchall4.User
import com.example.newchall4.databinding.FragmentProfileBinding

class ProfileFragment : Fragment() {

    private lateinit var binding: FragmentProfileBinding
    private val auth = FirebaseAuth.getInstance()
    private val database = FirebaseDatabase.getInstance()
    private val usersRef = database.getReference("users")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(inflater, container, false)

        val userId = auth.currentUser?.uid
        userId?.let { uid ->
            usersRef.child(uid).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        val user = snapshot.getValue(User::class.java)
                        user?.let {
                            binding.etUsernameValue.text = it.username
                            binding.tvEmailValue.text = it.email
                            binding.tvMobileValue.text = it.phone
                        }
                    }
                }

                override fun onCancelled(error: DatabaseError) {
                    // Crash here
                }
            })
        }

        // Logout
        binding.logoutButton.setOnClickListener {
            auth.signOut()
            startActivity(Intent(requireContext(), ActivityLoginAccount::class.java))
            requireActivity().finish()
        }

        return binding.root
    }
}