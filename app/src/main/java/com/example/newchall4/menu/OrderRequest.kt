package com.example.newchall4.menu

data class OrderRequest(
    val orders: List<Order>,
    val total: Int?,
    val username: String?
)