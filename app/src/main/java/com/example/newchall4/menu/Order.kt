package com.example.newchall4.menu

import com.google.gson.annotations.SerializedName

data class Order(
    val catatan: String?,
    val harga: Int?,
    val nama: String?,
    val qty: Int?
)