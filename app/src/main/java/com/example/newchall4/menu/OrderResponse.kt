package com.example.newchall4.menu

data class OrderResponse (
    val code: Int?,
    val message: String?,
    val status: Boolean?
)
