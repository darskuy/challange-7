package com.example.newchall4.inject

import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module
import com.example.newchall4.viewmodel.CartViewModel
import com.example.newchall4.database.CartDatabase
import com.example.newchall4.repository.CartRepository

object KoinModule {

    private val localModule = module {
        single { CartDatabase.getDatabase(androidContext()) }
        single { get<CartDatabase>().cartItemDao() }
    }

    private val repositoryModule = module {
        single { CartRepository(get()) }
    }

    private val viewModelModule = module {
        viewModel { CartViewModel(get()) }
    }

    val module: List<Module> = listOf(
        localModule,
        repositoryModule,
        viewModelModule
    )
}