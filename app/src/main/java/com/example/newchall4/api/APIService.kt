package com.example.newchall4.api

import com.example.newchall4.menu.MenuCategory
import com.example.newchall4.menu.MenuList
import com.example.newchall4.menu.OrderResponse
import com.example.newchall4.menu.OrderRequest
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.Call

interface APIService {
    @GET("listmenu")
    fun getListMenu(
    ): Call<MenuList>

    @GET("category-menu")
    fun getCategoryMenu(
    ): Call<MenuCategory>

    @POST("order-menu")
    fun order(
        @Body orderRequest: OrderRequest
    ): Call<OrderResponse>
}